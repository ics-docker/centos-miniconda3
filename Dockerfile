FROM centos:7.4.1708

LABEL maintainer "benjamin.bertrand@esss.se"

ENV LANG=en_US.UTF-8 LC_ALL=en_US.UTF-8 LANGUAGE=en_US.UTF-8

RUN yum update -y && yum install -y \
    which \
    bzip2 \
    wget \
    git \
  && yum clean all \
  && rm -rf /var/cache/yum

# Install tini
ENV TINI_VERSION v0.16.1
RUN curl -L -o /usr/local/bin/tini https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini \
  && chmod +x /usr/local/bin/tini

# Install conda
ENV CONDA_UID 48
ENV MINICONDA_INSTALLER_VERSION 4.4.10
ENV CONDA_VERSION 4.4.11
RUN curl -L -O "https://repo.continuum.io/miniconda/Miniconda3-${MINICONDA_INSTALLER_VERSION}-Linux-x86_64.sh" \
  && /bin/bash "Miniconda3-${MINICONDA_INSTALLER_VERSION}-Linux-x86_64.sh" -f -b -p /opt/conda \
  && rm "Miniconda3-${MINICONDA_INSTALLER_VERSION}-Linux-x86_64.sh" \
  && /opt/conda/bin/conda config --system --add pinned_packages defaults::conda \
  && /opt/conda/bin/conda config --system --add channels conda-forge \
  && /opt/conda/bin/conda config --system --add channels https://artifactory.esss.lu.se/artifactory/ics-conda \
  && /opt/conda/bin/conda config --system --set auto_update_conda false \
  && /opt/conda/bin/conda config --system --set show_channel_urls true \
  && /opt/conda/bin/conda install -y conda="${CONDA_VERSION}" \
  && /opt/conda/bin/conda clean -tipsy \
  && ln -s /opt/conda/etc/profile.d/conda.sh /etc/profile.d/conda.sh \
  && useradd -m -s /bin/bash -u "$CONDA_UID" -g users conda \
  && chown -R conda:users /opt/conda

ENV PATH /opt/conda/bin:$PATH

ENTRYPOINT [ "/usr/local/bin/tini", "--" ]
CMD ["/bin/bash"]

USER conda
