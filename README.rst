centos-miniconda3
=================

**DEPRECATED**

The repository was renamed *miniconda*. See https://gitlab.esss.lu.se/ics-docker/miniconda
The registry is left for backward compatibility. New images should use the registry.esss.lu.se/ics-docker/miniconda image.

-----------------------------------

Docker_ image with a bootstrapped installation of Miniconda_ (with Python 3.6) based on CentOS 7.

The official `docker pull continuumio/miniconda3 <https://hub.docker.com/r/continuumio/miniconda3/>`_ is based on debian:8.

Docker pull command::

    docker pull registry.esss.lu.se/ics-docker/centos-miniconda3:latest


.. _Docker: https://www.docker.com
.. _Miniconda: https://conda.io/miniconda.html
